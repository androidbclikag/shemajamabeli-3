package com.example.a3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_layout.view.*

class RecyclerViewAdapter(private val dataSet: ArrayList<UserModel.ItemModel.Area>) :
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val items = dataSet[adapterPosition]
            itemView.idTextView.text = items.areaId.toString()
            itemView.nameTextView.text = items.name
            itemView.countryCodeTextView.text = items.countryCode
            if (items.ensignUrl != null)
            Glide.with(itemView).load(items.ensignUrl).placeholder(R.mipmap.ic_launcher_round)
                .into(itemView.urlImageView)





        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
    )

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    }

}